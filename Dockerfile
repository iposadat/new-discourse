# Check https://github.com/discourse/discourse_docker/blob/master/launcher
FROM discourse/base:2.0.20211019-0324

ENV \
    # DBOD PostgreSQL version
    PG_MAJOR=11 \
    RUBY_ALLOCATOR=/usr/lib/libjemalloc.so.1 \
    HOME=/var/www/discourse/ \
    RAILS_ENV=production \
    DESCRIPTION="Platform for running Discourse instances" \
    PATH=/pups/bin/:${PATH}

LABEL maintainer="Discourse Administrators <discourse-admins@cern.ch>" \
      io.openshift.expose-services="8080,3000,6379:http" \
      description="$DESCRIPTION" \
      io.k8s.description="$DESCRIPTION" \
      name="Discourse for CERN"

COPY templates/ .

RUN \
    ## Install PG_11 for CERN
    ## ffi.:
    ## - https://github.com/discourse/discourse_docker/blob/master/templates/postgres.12.template.yml
    ## - https://github.com/discourse/discourse_docker/commit/82e4d76f8bb93a3517a71ebb0c710307101476be
    DEBIAN_FRONTEND=noninteractive apt-get purge -y postgresql-13 postgresql-client-13 postgresql-contrib-13 && \
    apt -y update && \
    apt -y install postgresql-${PG_MAJOR} postgresql-client-${PG_MAJOR} \
    postgresql-contrib-${PG_MAJOR} && \
    apt clean && \
    ## Discourse specific bits
    ## Drop the root user and change file and directory permissions.
    pups web.template.yml && \
    pups redis.template.yml && \
    CHECK_DIRS="/var/www/discourse/ \
                /etc/nginx/ \
                /var/lib/nginx/ \
                /etc/redis/ \
                /usr/local/etc/ \
                /etc/runit/ \
                /etc/service/" && \                
    mkdir -p $CHECK_DIRS && \
    chgrp -R 0 $CHECK_DIRS && \
    chmod -R g=u $CHECK_DIRS

WORKDIR /var/www/discourse/

### entrypoint will be overriden for init-containers, nginx and redis
ENTRYPOINT ["/etc/service/unicorn/run"]